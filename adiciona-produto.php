<?php
    require_once("cabecalho.php");
    require_once("logica-usuario.php");
    
    verificaUsuario();
    
    $categoria = new Categoria();
    $categoria -> setId($_POST["categoria_id"]);
    
    $nome = $_POST["nome"];
	$preco = $_POST["preco"];
	$descricao = $_POST["descricao"];
	$categoria = $categoria;
	$isbn = $_POST['isbn'];
	$tipoProduto = $_POST['tipoProduto'];
	
	if (array_key_exists("usado", $_POST)) {
	    $usado = "true";
	} else {
	    $usado = "false";
	}
	
	if ($tipoProduto == "Livro") {
	    
	    $produto = new Livro($nome, $preco, $descricao, $categoria, $usado);
	    $produto -> setIsbn($isbn);
	    
	} else {
	    
	    $produto = new Produto($nome, $preco, $descricao, $categoria, $usado);
	    
	}
	
	$ProdutoDAO = new ProdutoDAO($conexao);
	
	if ($ProdutoDAO -> insereProduto($produto)) { 
?>
	    <p class="text-success">O Produto <?=$produto -> getNome()?>, <?=$produto -> getPreco()?> foi adicionado.</p>
    
<?php } else {
		
        $msg = mysqli_error($conexao);
        
?>
		
		<p class="text-danger">O Produto <?=$produto -> getNome()?> nao foi adicionado: <?=$msg?></p>
		
<?php

	}    
	   mysqli_close($conexao);

?>

<?php include("rodape.php");?>