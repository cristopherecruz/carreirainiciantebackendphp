			<tr>
				<td>
					Nome
				</td>
				<td>
					<input type="text" name="nome" class="form-control" value="<?=$produto -> getNome()?>">
				</td>
			</tr>
			<tr>
				<td>
					Preco
				</td>
				<td>
					<input type="number" step="0.01" name="preco" class="form-control" value="<?=$produto -> getPreco()?>">
				</td>
			<tr>
				<td>
					Descricao
				</td>
				<td>
					<textarea name="descricao" class="form-control"><?=$produto -> getDescricao()?></textarea>
				</td>
			</tr>
			<tr>
				<td></td>
				<td>
					<input type="checkbox" name="usado" <?=$produto -> getUsado()?> value="true"> Usado
				</td>
			</tr>
			<tr>
				<td>
					Categoria
				</td>
				<td>
					<select name="categoria_id" class="form-control">
						<?php foreach ($categorias as $categoria) : 
					    
					        $categoriaSelecionada = $produto -> getCategoria() -> getId() == $categoria -> getId();
					        $selecaoCategoria = $categoriaSelecionada ? "selected='selected'" : "" ?>
						
							<option value="<?=$categoria -> getId()?>" <?=$selecaoCategoria?>>
								<?=$categoria -> getNome()?>
							</option>
						
						<?php endforeach; ?>
					</select>
				</td>
			</tr>
			<tr>
            	<td>Tipo do produto</td>
                <td>
                    <select name="tipoProduto" class="form-control">
                        <?php
                        
                            $tipos = array("Livro", "Produto");
                        
                            foreach($tipos as $tipo) : 
                                
                                $tipoSelecionado = get_class($produto) == $tipo;
                                $selecaoTipo = $tipoSelecionado ? "selected='selected'" : ""; ?>
                                
                            <option value="<?=$tipo?>" <?=$selecaoTipo?>>
                                <?=$tipo?>
                            </option>
                            
                        <?php endforeach; ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td>
                	ISBN (caso seja um Livro)
               	</td>
                <td>
                    <input type="text" name="isbn" class="form-control" value="<?php if ($produto -> temIsbn()) { echo $produto -> getIsbn(); } ?>" >
                </td>
            </tr>