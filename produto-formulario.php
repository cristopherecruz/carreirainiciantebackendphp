<?php
    
    require_once("cabecalho.php");
    require_once("logica-usuario.php");
    
    verificaUsuario();
    $CategoriaDAO = new CategoriaDAO($conexao);
    $categorias = $CategoriaDAO -> listaCategorias();
  
    $categoria = new Categoria();
    $categoria -> setId(1);
    
    $produto = new Produto("", "", "", $categoria, "");
    
?>

	<h1>Formulario de Produto</h1>

	<form action="adiciona-produto.php" method="post">
		<table class="table">
				
				<?php include "produto-formulario-base.php"; ?>
				
				<tr>
					<td>
						<input class="btn btn-primary" type="submit" value="Cadastrar">
					</td>
				</tr>
		</table>
	</form>

<?php include("rodape.php");?>