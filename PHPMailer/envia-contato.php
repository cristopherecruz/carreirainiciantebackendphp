<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;

session_start();

$nome = $_POST["nome"];
$email = $_POST["email"];
$mensagem = $_POST["mensagem"];

require_once("vendor/autoload.php");

$mail = new PHPMailer(true);

try {
    //Server settings
    //$mail -> SMTPDebug = 2;
    $mail -> isSMTP();
    $mail -> Host = 'smtp.gmail.com';
    $mail -> Port = 587;
    $mail -> SMTPSecure = 'tls';
    $mail -> SMTPAuth = true;
    $mail -> Username = 'emailtesteprogramacao@gmail.com';
    $mail -> Password = 'mudar123@';
    
    //Recipients
    $mail -> setFrom('emailtesteprogramacao@gmail.com', 'Loja');
    $mail -> addAddress('emailtesteprogramacao@gmail.com');
    
    //Content
    $mail -> isHTML(true);
    $mail -> Subject = 'email de contato da loja';
    $mail -> Body    = 'de: ' . $nome . ' <br/>email: ' . $email . ' <br/>mensagem: ' . $mensagem;
    $mail -> AltBody = 'de: ' . $nome . ' \nemail: ' . $email . ' \nmensagem: ' . $mensagem;
    
    $mail -> send();
    
    $_SESSION["success"] = "Mensagem enviada com sucesso";
    
    Header("Location: ../index.php");
    die();
    
} catch (Exception $e) {
    
    $_SESSION["danger"] = "Erro ao enviar mensagem " . $mail -> ErrorInfo;
    Header("Location: ../index.php");
    die();
    
}