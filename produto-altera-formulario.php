<?php
    
    require_once("cabecalho.php");
    
    $id = $_GET['id'];
    
    $ProdutoDAO = new ProdutoDAO($conexao);
    $CategoriaDAO = new CategoriaDAO($conexao);
    
    $produto = $ProdutoDAO -> buscaProduto($id);
    $categorias = $CategoriaDAO -> listaCategorias();
    
    $usado = $produto -> getUsado() ? "checked='checked'" : "";
    $produto -> setUsado($usado);
    
?>

	<h1>Alterando produto</h1>

	<form action="altera-produto.php" method="post">
		<input type="hidden" name="id" value="<?=$produto -> getId()?>">
		<table class="table">
			
			<?php include "produto-formulario-base.php"; ?>
			
			<tr>
				<td>
					<input class="btn btn-primary" type="submit" value="Alterar">
				</td>
			</tr>
		</table>
	</form>

<?php include("rodape.php");?>